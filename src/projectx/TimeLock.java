package projectx;
import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.WindowConstants;

public class TimeLock extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected JLabel timeLabel, timerLabel, user, pass, hr;
	protected int counter;
	protected JTextField enterTimeH, enterTimeM, enterTimeS, enterU;

	protected static JTextField enterP;
	protected JButton button, exitB;
	protected Timer timer;
	protected JPanel panel, panel1, panelTime, picPanel, timePanel;
	protected JRadioButton ra1, ra2;
	protected ButtonGroup group;
	protected JComboBox<String> comboBox;
	protected String[] hoursNum = { "1", "2", "3", "4", "5" };
	protected JMenuBar menuBar, menuBarStop;
	protected JMenu menuFile, menuStop;
	protected JMenuItem quit, changePass, stop;
	protected String passWord = "12345";
	protected JFrame screenSaverFrame, frameTime;
	protected TimeClass tc;

	protected JLabel imagearea;
	protected ImageIcon image;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public TimeLock() {

		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();

		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel1 = new JPanel();
		panel1.setLayout(new GridLayout(2, 2, 5, 5));

		timeLabel = new JLabel("Time : ", SwingConstants.CENTER);
		panelTime = new JPanel();
		panelTime.setLayout(new GridLayout(1, 9, 5, 5));
		panelTime.add(timeLabel);

		ra1 = new JRadioButton("เวลา");
		ra2 = new JRadioButton("เวลา");
		group = new ButtonGroup();
		group.add(ra1);
		ra1.setSelected(true);
		group.add(ra2);
		enterTimeH = new JTextField("ชั่วโมง", 2);
		enterTimeM = new JTextField("นาที", 2);
		enterTimeS = new JTextField("วินาที", 2);
		enterTimeH.setForeground(Color.BLUE);
		enterTimeM.setForeground(Color.BLUE);
		enterTimeS.setForeground(Color.BLUE);

		panelTime.add(ra1);
		hr = new JLabel("ชม.");
		comboBox = new JComboBox(hoursNum);

		panelTime.add(comboBox);
		panelTime.add(hr);
		panelTime.add(ra2);
		panelTime.add(enterTimeH);
		panelTime.add(enterTimeM);
		panelTime.add(enterTimeS);

		panel.add(panelTime, BorderLayout.NORTH);

		pass = new JLabel("Password : ", SwingConstants.CENTER);
		panel1.add(pass);

		enterP = new JTextField("Password");
		panel1.add(enterP);

		button = new JButton("Start");
		panel1.add(button);

		timerLabel = new JLabel("Waiting...", SwingConstants.CENTER);
		exitB = new JButton("Exit");
		panel1.add(exitB);

		panel.add(panel1, BorderLayout.CENTER);

		menuBar = new JMenuBar();

		menuFile = new JMenu("File");
		quit = new JMenuItem("Quit");
		changePass = new JMenuItem("Change Password");
		menuFile.add(changePass);
		menuFile.add(quit);

		menuBar.add(menuFile);

		setJMenuBar(menuBar);

		screenSaverFrame = new JFrame();
		picPanel = new JPanel();
		imagearea = new JLabel();
		image = new ImageIcon(getClass().getResource("/pic.jpg"));
		imagearea.setIcon(image);
		picPanel.add(imagearea);

		screenSaverFrame
				.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		screenSaverFrame.setUndecorated(true);
		screenSaverFrame.setResizable(false);
		screenSaverFrame.setVisible(true);
		screenSaverFrame.setSize(width, height);
		screenSaverFrame.add(picPanel);
		
		frameTime = new JFrame();
		frameTime.setLocation(1200, 625);
		frameTime.setSize(150, 95);
		frameTime.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		timePanel = new JPanel();
		timePanel.add(timerLabel);
		frameTime.add(timePanel);

		menuBarStop = new JMenuBar();
		menuStop = new JMenu("Log Out");
		stop = new JMenuItem("Stop");
		menuStop.add(stop);

		menuBarStop.add(menuStop);

		frameTime.setJMenuBar(menuBarStop);

		setContentPane(panel);
		setAlwaysOnTop(true);
		eventButton e = new eventButton();
		button.addActionListener(e);
		exitB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String out = JOptionPane.showInputDialog("Password");

				if (out.equals(passWord)) {
					System.exit(0);
				} else
					JOptionPane.showMessageDialog(null, "Password wrong");

			}
		});
		enterTimeH.addMouseListener(new checkMouse());
		enterTimeM.addMouseListener(new checkMouse());
		enterTimeS.addMouseListener(new checkMouse());
		enterP.addMouseListener(new checkMouse());
	}

	protected JPanel createPanelButton(JButton but, Color butColor) {
		but.setPreferredSize(new Dimension(100, 40));
		JPanel tempPanel = new JPanel();
		tempPanel.setBackground(butColor);
		tempPanel.add(but);
		return tempPanel;
	}

	protected class eventButton implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			if (passWord.equals(enterP.getText())) {
				int count = 0;
				try {
					if (ra1.isSelected())
						count = (comboBox.getSelectedIndex() + 1) * 3600;

					else if (ra2.isSelected())
						count = (int) (Double.parseDouble(enterTimeH.getText()))
								* 3600
								+ (int) (Double.parseDouble(enterTimeM
										.getText()))
								* 60
								+ (int) (Double.parseDouble(enterTimeS
										.getText()));

					frameTime.setVisible(true);
					tc = new TimeClass(count);
					timer = new Timer(1000, tc);
					timer.start();
					enterP.setText("*****");

				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, " Please enter number ");
				} finally {

					enterTimeH.setText("");
					enterTimeS.setText("");
					enterTimeM.setText("");
				}

			} else
				JOptionPane.showMessageDialog(null, "Password wrong");
		}
	}

	protected void enableKeyboard() {

		menuFile.setMnemonic(KeyEvent.VK_F);
		setMKeys(changePass, KeyEvent.VK_C, KeyEvent.VK_C, new ListListener());
		setMKeys(quit, KeyEvent.VK_X, KeyEvent.VK_Q, new ListListener());

	}

	protected void setMKeys(JMenuItem menu, int mKey, int aKey,
			ActionListener listener) {
		menu.setMnemonic(mKey);
		menu.setAccelerator(KeyStroke.getKeyStroke(aKey, ActionEvent.CTRL_MASK));
		menu.addActionListener(listener);
	}

	protected class ListListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == quit) {
				String out = JOptionPane.showInputDialog("Password");

				if (out.equals(passWord)) {
					System.exit(0);
				} else
					JOptionPane.showMessageDialog(null, "Password wrong");
			}

			if (e.getSource() == changePass) {
				String oldPassword = JOptionPane.showInputDialog(" Password",
						"");
				if (oldPassword.equals(passWord)) {
					String newPassword1 = JOptionPane.showInputDialog(
							"New Password", "");
					String newPassword2 = JOptionPane.showInputDialog(
							"Confirm new Password", "");
					if (newPassword1.equals(newPassword2)) {
						passWord = newPassword1;
					} else
						JOptionPane.showMessageDialog(null,
								"  Password no match  ");
				} else
					JOptionPane.showMessageDialog(null, "Password wrong");
			}
		}
	}

	protected class TimeClass implements ActionListener {
		int counter;
		int hours = counter / 3600;
		int minute = (counter % 3600) / 60;
		int second = counter % 60;

		protected TimeClass(int counter) {
			this.counter = counter;
		}

		public void actionPerformed(ActionEvent tc) {

			counter--;
			hours = counter / 3600;
			minute = (counter % 3600) / 60;
			second = counter % 60;

			String hZero = "";
			if (hours < 10)
				hZero = "0";
			String mZero = "";
			if (minute < 10)
				mZero = "0";
			String sZero = "";
			if (second < 10)
				sZero = "0";

			String stringT = hZero + hours + " : " + mZero + minute + " : "
					+ sZero + second;

			stop.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {

					counter = 0;

				}
			});

			if (counter >= 1) {
				timerLabel.setText("Time left: " + stringT);
				screenSaverFrame.setVisible(false);
				setVisible(false);

				if (counter <= 300) {
					timerLabel.setForeground(Color.RED);
				}
			} else {
				timer.stop();
				frameTime.setVisible(false);
				enterTimeH.setText("ชั่วโมง");
				enterTimeM.setText("นาที");
				enterTimeS.setText("วินาที");
				enterP.setText("Password");
				screenSaverFrame.setVisible(true);
				screenSaverFrame.setAlwaysOnTop(true);
				screenSaverFrame.setAlwaysOnTop(false);
				timerLabel.setForeground(Color.BLACK);
				timerLabel.setText("Waiting...");
				setVisible(true);

				Toolkit.getDefaultToolkit().beep();
			}
		}
	}

	public static void main(String args[]) {
		TimeLock time = new TimeLock();
		time.enableKeyboard();
		time.setUndecorated(true);
		time.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		time.setSize(800, 150);
		time.setTitle("Time");
		time.pack();
		time.setVisible(true);

	}

	
	private class checkMouse implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getSource() == enterTimeH) {
				enterTimeH.setText("");
			} else if (e.getSource() == enterTimeM) {
				enterTimeM.setText("");
			} else if (e.getSource() == enterTimeS) {
				enterTimeS.setText("");
			} else if (e.getSource() == enterP) {
				enterP.setText("");

			}

		}

		@Override
		public void mouseEntered(MouseEvent arg0) {

		}

		@Override
		public void mouseExited(MouseEvent arg0) {

		}

		@Override
		public void mousePressed(MouseEvent arg0) {

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {

		}

	}

}